from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Blog, Comment
from common.json import ModelEncoder
import json

class CommentListEncoder(ModelEncoder):
    model = Comment
    properties = ["content"]

class BlogListEncoder(ModelEncoder):
    model = Blog
    properties = ["title"]

class BlogDetailEncoder(ModelEncoder):
    model = Blog
    properties = ["title", "content",]

@require_http_methods(["GET", "POST"])
def list_blogs(request):
    if request.method == "GET":
        # Get all of the blogs
        blogs = Blog.objects.all()
        # Return the blogs in a JsonResponse
        return JsonResponse({"blogs": blogs}, encoder=BlogListEncoder)
    else:
        # Convert the post to a dictionary
        content = json.loads(request.body)
        # Use that dictionary to create a new blog
        blog = Blog.objects.create(**content)
        # Return the new blog information
        return JsonResponse({"blog": blog}, encoder=BlogListEncoder)

def show_blog_detail(request, pk):
    # Get the blog
    blog = Blog.objects.get(pk=pk)
    # Return the JsonResponse
    return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)

def create_comment(request, blog_id):
    if request.method == "POST":
        blog = Blog.objects.get(id=blog_id)
        content = json.loads(request.body)
        Comment.objects.create(
            content=content["comment"],
            blog=blog,
        )
        return JsonResponse({"message": "created"})
